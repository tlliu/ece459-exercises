// You should implement the following function
fn fibonacci_number(n: u32) -> u32 {
    if n < 3 {
        return 1
    }
    let mut array: Vec<_> = vec![1; (n+1) as usize];
    for i in 2..n {
        array[i as usize] = array[(i-1) as usize] + array[(i-2) as usize];
    }
    array[(n-1) as usize]
}


fn main() {
    println!("{}", fibonacci_number(10));
}

use std::sync::mpsc;
use std::thread;
use std::time::Duration;

// You should modify main() to spawn threads and communicate using channels
fn main() {
    let (tx, rx) = mpsc::channel();

    let tx1 = mpsc::Sender::clone(&tx);
    thread::spawn(move || {
        thread::sleep(Duration::from_secs(1));
        tx1.send("hello world").unwrap();
    });

    drop(tx);
    
    for received in rx {
        println!("Got: {}", received);
    }
}

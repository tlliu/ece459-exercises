use std::thread;
// use std::time::Duration;

static N: i32 = 10;  // number of threads to spawn

// You should modify main() to spawn multiple threads and join() them
fn main() {
    let mut children = vec![];
    for _ in 0..N {
        children.push(
            thread::spawn(|| {
                for i in 0..5 {
                    println!("{}", i);
                    // thread::sleep(Duration::from_millis(1))
                }
            })
        );
    }
    for t in children {
        t.join().unwrap();
    }
}
